// Imports the Google Cloud client library
const speech = require('@google-cloud/speech');
// Creates a client
const client = new speech.SpeechClient();

/**
 * TODO(developer): Uncomment the following lines before running the sample.
 */

const config = {
    // encoding:"FLAC",
    sampleRateHertz: 8000,
    channels:1,
    // sampleRateHertz: 8000,
    enableSpeakerDiarization: true,
    diarizationSpeakerCount: 1,
    languageCode: 'en-IN',
    speechContexts: [{
      phrases: ["welcome to dmi finance","am i audible"]
    }],
    enable_automatic_punctuation:true,
    use_enhanced:true
};

const audio = {
    // uri: "gs://breeze-absa/ITSD1.wav",
    // uri: "gs://breeze-dmi-recordings/test3-1.flac",
    // uri: "gs://breeze-dmi-recordings/test3-2.flac", // volume
    // uri: "gs://breeze-dmi-recordings/test3-3 noice reduction.flac", // noice
    // uri: "gs://breeze-dmi-recordings/test3-3.flac", // noice + volume
    // uri: "gs://breeze-dmi-recordings/test3-4.flac", // noice + volume
    // uri: "gs://breeze-dmi-recordings/test3-4-1.flac", // noice + volume
    uri: "gs://breeze-dmi-recordings/AudioOptimized/07503393616_DMI013@MattsenKumar.com_2020-03-04-10-50-12.flac", // noice + volume
    // uri: "gs://breeze-dmi-recordings/test3-4-1.flac", // noice + volume
    // uri: "gs://breeze-dmi-recordings/07389131007_DMI001@MattsenKumar.com_2020-04-12-15-47-39.mp3",
    // uri: "gs://breeze-dmi-recordings/01615008740_DMI022@MattsenKumar.com_2020-03-31-13-29-52.mp3",
  };

const request = {
  config,
  audio,
};

const quickstart = async () => {
    try {
        const [operation] = await client.longRunningRecognize(request);
        // Get a Promise representation of the final result of the job

        const [response] = await operation.promise();
        const transcription = response.results
          .map(result => result.alternatives[0].transcript)
          .join('\n');
        console.log(`Transcription: ${transcription}`);
    }
    catch (err) {
        console.log(err)
    }
}
quickstart()